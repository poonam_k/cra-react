import React from 'react';
import { Router, Route, browserHistory } from 'react-router'
// Import components to route
import App from '../App';
import HomeComponent from '../components/Home/home';

const styles = {
    card: {
      border: '1px solid #eeeeee',
      borderRadius: '3px',
      padding: '15px',
      width: '250px'
    },
    image: {
      height: '200px',
      width: '250px'
    }
};

export default(
            <Router history={browserHistory}>

              <Route  component={App}>
                <Route path="/" component={HomeComponent} styles={styles}></Route>
    
              </Route>
            </Router>
);
