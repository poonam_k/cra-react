import React, { Component } from 'react';
import './App.css';
import Header from './components/common/header';
import sidenav from './components/common/sidenav';
import Footer from './components/common/footer';

class App extends Component {
  
  render() {
    return (
      <div>
          <Header />
		 {/*<sidenav />*/}
          <div className="main-container" id="wrapper">
            {this.props.children}
          </div>
          
      </div>
    );
  }
}

export default App;
