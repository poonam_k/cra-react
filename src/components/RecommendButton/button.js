import React, { Component } from "react";
import { TweenMax } from "gsap";
import More from './more';
import Transition from "react-transition-group";
import TransitionGroup from "react-addons-transition-group";
import CopyToClipboard from 'react-copy-to-clipboard';

export default class Button extends Component {

  constructor() {
    super();
    this.state = {
      showMore: false,
      copied: false,
  };
     this.showToggle = this.showToggle.bind(this);
  }

  showToggle() {
    this.setState({ showMore: !this.state.showMore });

	}
  componentWillEnter ( callback ) {
    const el = this.container;
    // TweenMax.fromTo(el, 0.3, {y: 100, opacity: 0}, {y: 0, opacity: 1, onComplete: callback});
    TweenMax.fromTo(el, 0.3, { x: 100, opacity: 0 }, { x: 0, opacity: 1, onComplete: callback });
  }

  componentWillLeave (callback) {
    const el = this.container;
    TweenMax.fromTo(el, 0.3, { x: 0, opacity: 1 }, { x: -100, opacity: 0, onComplete: callback } );
  }
  render() {
    console.log('show more :: ', this.state.showMore);
    console.log('props here are :: ', this.props);
    return (   
      <div>
      <TransitionGroup>
        {this.state.showMore && this.props.toggle && <More/>}
        {!this.state.showMore && this.props.toggle && <div className="testDiv" ref={c => this.container = c}>
          
 <div className="social-dv">
              <a href="javascript:void(0)"><span><img alt="fb" src={require('../common/images/fb1.png')} className="log-img" /><span className="social-txt">Facebook</span></span></a>
              <a href="javascript:void(0)"><span><img alt="tweet" src={require('../common/images/tweet.png')} className="log-img" /><span className="social-txt">Twitter</span></span></a>
              <a href="javascript:void(0)"><span><img alt="email" src={require('../common/images/email.png')} className="log-img" /><span className="social-txt">Email</span></span></a>
              <a href="javascript:void(0)"><span><img alt="more" src={require('../common/images/more.png')} className="log-img" onClick={this.showToggle}/><span className="social-txt">More</span></span></a>
            </div>
            <CopyToClipboard text="copy to clipboard success"
          onCopy={() => this.setState({copied: true})}>
         <a href="javascript:void(0)" className="clip-link">Copy link</a>
        </CopyToClipboard>
        
       </div>
     } 
      </TransitionGroup>
      </div>
      
    );
  }
}

