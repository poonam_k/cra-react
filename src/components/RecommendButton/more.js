import React, { Component } from "react";
import { TweenMax } from "gsap";
import CopyToClipboard from 'react-copy-to-clipboard';


export default class More extends Component {
  constructor() {
    super();
    this.state = {
      copied: false,
    };
  }
  componentWillEnter ( callback ) {
    const el = this.container;
    // TweenMax.fromTo(el, 0.3, {y: 100, opacity: 0}, {y: 0, opacity: 1, onComplete: callback});
    TweenMax.fromTo(el, 0.3, { x: 100, opacity: 0 }, { x: 0, opacity: 1, onComplete: callback });
  }

  componentWillLeave (callback) {
    const el = this.container;
    TweenMax.fromTo(el, 0.3, { x: 0, opacity: 1 }, { x: -100, opacity: 0, onComplete: callback } );

  }
  render() {
    return (
      <div className="testDiv" ref={c => this.container = c}>
       <a href="javascript:void(0)"><img alt="google" src={require('../common/images/gplus.png')} className="log-img" /><span className="social-txt">Google +</span></a>
        <a href="javascript:void(0)"><img alt="linkedin" src={require('../common/images/linked.png')} className="log-img" /><span className="social-txt">Linkedin</span></a>    
        <CopyToClipboard text="copy to clipboard success"
          onCopy={() => this.setState({copied: true})}>
           <a href="javascript:void(0)" className="clip-link">Copy Link</a>
        </CopyToClipboard>
        
      </div>
    );
  }
}
