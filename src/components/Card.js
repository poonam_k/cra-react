import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactCardFlip from 'react-card-flip';
import './Card.css';
import { fetchData } from '../../actions/index';
import Transition from "react-transition-group";
import TransitionGroup from "react-addons-transition-group";
import TestDiv from "../../components/RecommendButton/test.js";
import Button from "../../components/RecommendButton/button.js";


class Example extends Component  {
  constructor() {
    super();
    let cosntData = fetchData();
    this.state = {
      isFlipped: false,
      cardData: cosntData.datas,
      shouldShowBox: true,
    };
     this.toggleBox = this.toggleBox.bind(this);
  }

     toggleBox(key) {
    console.log("i am here");
    this.setState({
      shouldShowBox: !this.state.shouldShowBox,
    });
    console.log('the value of shouldShowBox is ',this.state.shouldShowBox)
     let tmpArr = this.state.cardData
    tmpArr[key].btnshow = !tmpArr[key].btnshow;
    this.setState({btnclicked:tmpArr}, function() {
      console.log('btnclicked :: ',this.state.btnclicked);
    })
  };

    handleClick(key) {
    let tmpArr = this.state.cardData
    tmpArr[key].readmore = !tmpArr[key].readmore;
    this.setState({cardData:tmpArr}, function() {
    })
		this.setState({ isFlipped: !this.state.isFlipped });
  }
  
  trimmedString(val,key)
  {
    let maxLength = 20
    if (val.length>maxLength){ 
      let trimmedString = val.substr(0, maxLength);
      trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")))
      return (<label>{trimmedString} <a href="#" onClick={this.handleClick.bind(this,key)}> Read More </a></label>);
     }
    else{
      return val
    }

  }

  renderData() {
    let aa = this.state.cardData;
    return aa.map((post,key) =>{
      return (
        <div className="Offer" >
        <ReactCardFlip isFlipped={post.readmore}>
       {!post.readmore && <div key="front">
          
          <div className="card" style= {{width: '20rem' }}>
            <div className = 'image'>
              <img className="card-img-top" src="56c1c27ed6d01c2cc11a36ec89816f31" alt="Card image cap"/>
              <div className = "frontlayer">
                <span>
                    {key } ...15 $ per lead
                  </span>
              </div>
            </div>
            <div className="card-block">
              <h4 className="card-title">{post.name}</h4>
              <p className="card-text">
                {console.log('length value is : ', post.detail)}
                {this.trimmedString(post.detail,key)}

              </p>
              <div className='btn'>
              <TransitionGroup>
            { !post.btnshow && <TestDiv  toggle = {this.toggleBox.bind(this,key)} />}
           { post.btnshow && <Button   toggle={this.toggleBox.bind(this,key)} />}
          </TransitionGroup>
          </div>
            </div>
          </div> 
            
            
          </div>}

          {post.readmore && <div key="back">
            herhererer
            <div className="Offer">
              <div className="card" style= {{width: '20rem' }}>
                <div>
                  <h1> More info of {post.id} </h1>
                </div>
              </div>
            </div>
            <button onClick={this.handleClick.bind(this,key)}>
              Flip Card
            </button>
          </div>}
      </ReactCardFlip>
      </div>
    );
        })

  }


  render() {
    return (
            <ul className="list-group">
                {this.renderData()}
            </ul>  
    );
  }
};


export default Example;
