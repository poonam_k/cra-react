import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import { Router, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';
import promise from 'redux-promise';
import routes from './router';
import thunkMiddleware from 'redux-thunk';

const createStoreWithMiddleware = applyMiddleware( promise, thunkMiddleware )( createStore );

ReactDOM.render(
  <Provider store={ createStoreWithMiddleware(reducers) }>
    <Router history={browserHistory} routes={ routes } />
  </Provider>, document.getElementById( 'root' ));
  
  
  

